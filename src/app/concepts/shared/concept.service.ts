import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { Concept } from './concept';

@Injectable({
  providedIn: 'root'
})
export class ConceptService {
  apiUrl = "http://localhost:3000/api/v1/skills/"
  constructor(private http: HttpClient) { }

  getSkillconcepts(skill_id:number):Observable<Concept[]> {
    return this.http.get<Concept[]>(`${this.apiUrl}/${skill_id}/concepts`)
    .pipe(
      tap(skills => console.log('fetched Concept')),
      catchError(this.handleError('getSkills', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error("ERROR :", error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.info(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
