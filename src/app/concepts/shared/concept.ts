export class Concept {
  id:number;
  title:string;
  desc:string;
  ref_url:string;
}
