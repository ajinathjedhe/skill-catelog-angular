import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConceptService } from './shared/concept.service';
import { Concept } from './shared/concept';

@Component({
  selector: 'app-concepts',
  templateUrl: './concepts.component.html',
  styleUrls: ['./concepts.component.css']
})
export class ConceptsComponent implements OnInit {

  concepts: Concept[];

  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private conceptService:ConceptService
  ) { }

  ngOnInit() {
    this.getSkillconcepts();
  }

  getSkillconcepts(){
    this.conceptService.getSkillconcepts(this.getSkillId()).subscribe(
      concepts => this.concepts = concepts
    )
  }
  getSkillId():number{
    return parseInt(this.route.snapshot.paramMap.get('id'));
  }
}
