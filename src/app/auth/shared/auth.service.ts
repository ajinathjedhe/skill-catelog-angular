import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService} from 'ngx-cookie-service'
import { Store } from '@ngrx/store'

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private apiUrl = 'http://localhost:3000/api/';
  private httpHeader = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  isLoggedIn = false;

  constructor(
    private http: HttpClient,
    private cookieService:CookieService,
    private store: Store<any>) {
      this.setLoggedInStatus()
    }

  getUserDetails(params:any){
    return this.http.post(`${this.apiUrl}/login`, params, this.httpHeader)
  }

  setLoggedInStatus(){
    var token = this.cookieService.get('token')
    if(!!token)
      this.isLoggedIn = true
  }

  getAllState() { 
    return this.store.select('appReducer') 
  }
}
