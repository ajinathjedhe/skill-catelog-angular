import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AddHeaderInterceptor implements HttpInterceptor {
  constructor( private cookieService:CookieService){
  }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Clone the request to add the new header
    var token = this.cookieService.get('token')
    const clonedRequest = req.clone({ headers: req.headers.set('Authorization', token) });
    // Pass the cloned request instead of the original request to the next handle
    return next.handle(clonedRequest);
  }
}