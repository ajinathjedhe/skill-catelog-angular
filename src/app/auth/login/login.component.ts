import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { User } from '../shared/user';
import { AuthService } from '../shared/auth.service';
import { CookieService} from 'ngx-cookie-service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user:User = new User();
  constructor( 
    private auth: AuthService,
    private cookieService:CookieService,
    private router: Router) { }

  ngOnInit() {
  }

  doLogin(event){
    var params:any = this.setUserDetail(event);
    this.auth.getUserDetails(params).subscribe( response => {
      if(response['status'] == "ok"){ 
        console.log(response)
        this.auth.isLoggedIn = true
        this.cookieService.set('token', response["token"] );
        this.router.navigate(['skills']);
      } else {
        console.log("Invalid Credential :", response['error'])
        this.router.navigate(['login']);
      }
    });
  } 
  
  setUserDetail(event):any{
    var email:string = event.target.querySelector("#email").value
    var password:string = event.target.querySelector("#password").value
    return { auth: { "email": email, "password": password } }
  }
}
