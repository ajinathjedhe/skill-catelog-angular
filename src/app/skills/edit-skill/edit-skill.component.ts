import { Component, OnInit } from '@angular/core';
import { SkillService } from '../shared/skill.service';
import { ShowSkillComponent } from "../show-skill/show-skill.component"
import { Skill } from '../shared/skill.model';

@Component({
  selector: 'app-edit-skill',
  templateUrl: './edit-skill.component.html',
  styleUrls: ['./edit-skill.component.css'],
  providers: [ SkillService ]
})

export class EditSkillComponent extends ShowSkillComponent { 
  skill:Skill

  updateSkill(){
    this.skillService.updateSkill(this.skill).subscribe(
    data => {
      console.log("Skill Updated");
      this.router.navigateByUrl("/skills");
    },
    error => {
      console.error("Error Updating record");
    });
  }
}
