import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SkillService } from '../shared/skill.service';
import { Skill } from '../shared/skill.model';

@Component({
  selector: 'app-show-skill',
  templateUrl: './show-skill.component.html',
  styleUrls: ['./show-skill.component.css']
})
export class ShowSkillComponent implements OnInit {
  skill: Skill;

  constructor(
    protected skillService: SkillService,
    private route: ActivatedRoute,
    protected router: Router
  ){}

  ngOnInit():void {
    this.getSkill()
  }

  getSkill():void{
    this.skillService.getSkill(this.getSkillId()).subscribe(
      skill => this.skill = skill
    )
  }

  deleteSkill():void{
   this.skillService.deleteSkill(this.getSkillId()).subscribe(
    data => {
      console.log("Skill Deleted");
      this.router.navigateByUrl("/skills");
    },
    error => {
      console.error("Error Deleting record");
    });
  }

  getSkillId():number{
    return parseInt(this.route.snapshot.paramMap.get('id'));
  }
}
