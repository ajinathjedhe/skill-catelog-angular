import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ShowSkillComponent } from './show-skill.component';
import { ConceptsComponent } from '../../concepts/concepts.component';
import { HttpClientModule } from '@angular/common/http'; 

describe('ShowSkillComponent', () => {
  let component: ShowSkillComponent;
  let fixture: ComponentFixture<ShowSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowSkillComponent, ConceptsComponent ],
      imports: [RouterTestingModule, HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
