import { Component, OnInit } from '@angular/core';
import { SkillService } from './shared/skill.service';
import { AuthService } from '../auth/shared/auth.service'
import { Skill } from './shared/skill.model'
  
@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  skills: Skill[];
  skill: Skill;

  constructor(
    private skillService: SkillService,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.getSkills();
  }

  getSkills(){
    this.skillService.getSkills().subscribe(
      skills => this.skills = skills
    );
  }
}



