import { Component, OnInit } from '@angular/core';
import { Skill } from '../shared/skill.model';
import { SkillService } from '../shared/skill.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-new-skill',
  templateUrl: './new-skill.component.html',
  styleUrls: ['./new-skill.component.css']
})
export class NewSkillComponent implements OnInit {
  skill:Skill = new Skill();
  
  constructor(
    private skillService:SkillService,
    private router: Router
  ){}

  ngOnInit():void {
  }
  
  CreateSkill(){
    this.skillService.createSkill(this.skill).subscribe(
    data => {
      console.log("Skill Deleted");
      this.router.navigateByUrl("/skills");
    },
    error => {
      console.error("Error Deleting record");
    });
  }
}
