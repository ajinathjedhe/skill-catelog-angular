import { Component, OnInit, Input } from '@angular/core';
import { Skill } from '../shared/skill.model';
import { SkillService } from '../shared/skill.service';

@Component({
  selector: 'app-skill-form',
  templateUrl: './skill-form.component.html',
  styleUrls: ['./skill-form.component.css']
})
export class SkillFormComponent implements OnInit {
  
  @Input() skill: Skill;
  skills: Skill[];
  isFramework: boolean;

  constructor(
    private skillService: SkillService
  ) {}

  ngOnInit() {
    this.getLanguageDropDown();
    this.isFramework = false
  }

    getLanguageDropDown(){
      this.skillService.getLanguages().subscribe(
        skills => this.skills = skills
      );
    }
}
