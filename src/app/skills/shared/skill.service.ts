import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { Skill } from "./skill.model";

@Injectable({
  providedIn: 'root'
})

export class SkillService {
  private apiUrl = 'http://localhost:3000/api/v1/skills';
  private httpHeader = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
 
  constructor( private http: HttpClient) { 
  }

  getSkills():Observable<Skill[]> {
    return this.http.get<Skill[]>(this.apiUrl)
    .pipe(
      tap(skills => this.log('fetched skills')),
      catchError(this.handleError('getSkills', []))
    );
  }

  getLanguages():Observable<Skill[]>{
    return this.http.get<Skill[]>(`${this.apiUrl}/languages`)
    .pipe(
      tap(skills => this.log('fetched languages')),
      catchError(this.handleError('get Languages', []))
    );
  }

  getSkill(id: number): Observable<Skill>{
    return this.http.get<Skill>(`${this.apiUrl}/${id}`)
    .pipe(
      tap(skill => this.log("Skill Fetched")),
      catchError(this.handleError<Skill>(`getSkill id=${id}`))
    );
  }

  updateSkill(skill: Skill): Observable<Skill> {
    return this.http.put(`${this.apiUrl}/${skill.id}`, skill, this.httpHeader).pipe(
      tap(_ => this.log(`updated Skill id=${skill.id}`)),
      catchError(this.handleError<any>('Skill Updated'))
    );
  }

  createSkill(skill: Skill):Observable<Skill>{
    return this.http.post(this.apiUrl, skill, this.httpHeader).pipe(
      tap(_ => this.log(`new Skill created id=${skill.id}`)),
      catchError(this.handleError<any>('Skill Created'))
    );
  }

  deleteSkill(skillId: number){
    return this.http.delete(`${this.apiUrl}/${skillId}`, this.httpHeader).pipe(
      tap(_ => this.log(`Deleted Skill id=${skillId}`)),
      catchError(this.handleError<any>('Skill Deleted'))
    );
  }

  private log(message: string) {
    console.info(`Skill Service: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error("ERROR :", error); // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
