export class Skill {
  id: number;
  name: string;
  desc: string;
  language_id: number;

  constructor(name?:string, desc?:string){
    this.name = name;
    this.desc = desc;
  }
}
