import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http'; 
import { SkillService } from './skill.service';

describe('SkillService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkillService],
      imports:[HttpClientModule]
    });
  });

  it('should be created', inject([SkillService], (service: SkillService) => {
    expect(service).toBeTruthy();
  }));
});
