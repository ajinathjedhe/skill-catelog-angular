import { NgModule }             from '@angular/core';
import { RouterModule, Routes, CanActivate  } from '@angular/router';

import { SkillsComponent } from './skills/skills.component';
import { ShowSkillComponent } from './skills/show-skill/show-skill.component';
import { EditSkillComponent } from './skills/edit-skill/edit-skill.component';
import { NewSkillComponent } from './skills/new-skill/new-skill.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent} from './auth/registration/registration.component';

// Guard
import { AuthGuard } from './auth/shared/auth.guard';

const routes: Routes = [
  { path: '', component: SkillsComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent},
  { path: 'skills', component: SkillsComponent, canActivate: [AuthGuard]},
  { path: 'skills/new', component: NewSkillComponent, canActivate: [AuthGuard] },
  { path: 'skills/:id', component: ShowSkillComponent, canActivate: [AuthGuard]},
  { path: 'skills/:id/edit', component: EditSkillComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],  
  exports: [],
  providers: [AuthGuard]
})

export class AppRoutingModule {}
