import { ActionReducerMap } from '@ngrx/store'
import { reducer, appReducerState } from './skills.reducer'

interface AppState { 
  appReducer: appReducerState
}

export const reducers: ActionReducerMap<AppState>= { 
  appReducer: reducer
} 
