import { Component, OnInit } from '@angular/core';
import { AuthService} from '../auth/shared/auth.service'
import { CookieService} from 'ngx-cookie-service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  LoggedIn:boolean = false;
  
  constructor(
    private authService: AuthService,
    private cookieService: CookieService,
    private router:Router
  ) { console.log("here in code")}

  ngOnInit() {
    this.LoggedIn = this.authService.isLoggedIn;
  }

  // logout(){
  //   this.cookieService.deleteAll();
  //   this.LoggedIn = false;
  //   this.router.navigate(['login']);
  // }

  logout() { 
    debugger
    this.authService.getAllState()
  }

}
