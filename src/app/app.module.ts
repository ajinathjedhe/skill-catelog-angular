// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from "@angular/common/http";
import { FormsModule }   from  '@angular/forms';
import { RouterModule } from '@angular/router'
// Component
import { AppComponent } from './app.component';
import { SkillsComponent } from './skills/skills.component';
import { HeaderComponent } from './header/header.component';
import { ShowSkillComponent } from './skills/show-skill/show-skill.component';
import { AppRoutingModule } from './/app-routing.module';
import { ConceptsComponent } from './concepts/concepts.component';
import { EditSkillComponent } from './skills/edit-skill/edit-skill.component';
import { NewSkillComponent } from './skills/new-skill/new-skill.component';
import { SkillFormComponent } from './skills/skill-form/skill-form.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';

import { AddHeaderInterceptor } from './auth/shared/http.interceptors'
import { CookieService } from 'ngx-cookie-service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Integrate reducer 
import { StoreModule } from '@ngrx/store'
// Import all reducers
import { reducers } from './store/reducers'
 

@NgModule({
  declarations: [
    AppComponent,
    SkillsComponent,
    HeaderComponent,
    ShowSkillComponent,
    ConceptsComponent,
    EditSkillComponent,
    NewSkillComponent,
    SkillFormComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    StoreModule.forRoot(reducers, {})
  ],
  providers: [CookieService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AddHeaderInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
